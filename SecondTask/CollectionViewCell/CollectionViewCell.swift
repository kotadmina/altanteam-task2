//
//  CollectionViewCell.swift
//  SecondTask
//
//  Created by Sher Locked on 08.11.17.
//  Copyright © 2017 sherlocked. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var weightLabel: UILabel!
    
    func configure(weight: String) {
        self.weightLabel.text = weight
    }

}
