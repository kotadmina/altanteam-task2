//
//  FirstViewController.swift
//  SecondTask
//
//  Created by Sher Locked on 07.11.17.
//  Copyright © 2017 sherlocked. All rights reserved.
//

import UIKit
import RealmSwift

class FirstViewController: UIViewController {

    @IBOutlet weak var numberPickerView: UIPickerView!
    @IBOutlet weak var matrixViewPlaceholder: UIView!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let pickerData = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberPickerView.dataSource = self
        numberPickerView.delegate = self
        resultLabel.isHidden = true
        activityIndicator.hidesWhenStopped = true
    }

    @IBAction func calculateButtonPressed(_ sender: UIButton) {
        resultLabel.isHidden = true
        activityIndicator.startAnimating()
        for subview in matrixViewPlaceholder.subviews {
            subview.removeFromSuperview()
        }
        let selectedPickerItem = numberPickerView.selectedRow(inComponent: 0)
        let size = Int(pickerData[selectedPickerItem])!
        let matrix = MatrixRandomizer.createRandomMatrix(size: size, symmetric: false)
        let matrixCollectionView = MatrixCollectionView.instanceFromNib()
        matrixCollectionView.size = size
        matrixCollectionView.values = matrix
        matrixCollectionView.frame = matrixViewPlaceholder.bounds
        matrixViewPlaceholder.addSubview(matrixCollectionView)
        let dispatchQueue = DispatchQueue.global(qos: .utility)
        dispatchQueue.async {
            let result = TSPSolver.solve(with: matrix)
            let realmResult = TSPDataRealm(data: result)
            let realm = try! Realm()
            try! realm.write {
                realm.add(realmResult)
            }
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                let resultString = self.createResultString(result: result)
                self.resultLabel.isHidden = false
                self.resultLabel.text = resultString
                matrixCollectionView.result = result.resultArray
                matrixCollectionView.collectionView.reloadData()
            }
        }
        
    }
    
    private func createResultString(result: TSPData) -> String {
        var resultArrayString = result.resultArray.map { "\($0)" }
        if resultArrayString.count > 1 {
            resultArrayString.append(resultArrayString.first!)
        }
        let resultString = resultArrayString.joined(separator: " -> ")
        return resultString
    }
}

extension FirstViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
}

extension FirstViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
}
