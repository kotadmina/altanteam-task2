//
//  InfoViewController.swift
//  SecondTask
//
//  Created by Sher Locked on 07.11.17.
//  Copyright © 2017 sherlocked. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var matrixViewPlaceholder: UIView!
    @IBOutlet weak var resultLabel: UILabel!
    
    var result: TSPData!
    
    var matrixCollectionView: MatrixCollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        matrixCollectionView = MatrixCollectionView.instanceFromNib()
        matrixCollectionView.frame = matrixViewPlaceholder.bounds
        matrixViewPlaceholder.addSubview(matrixCollectionView)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        matrixCollectionView.size = result.size
        matrixCollectionView.values = result.aMatrix
        matrixCollectionView.result = result.resultArray
        matrixCollectionView.collectionView.reloadData()
        resultLabel.text = createResultString()
    }
    
    
    private func createResultString() -> String {
        var resultArrayString = result.resultArray.map { "\($0)" }
        if resultArrayString.count > 1 {
            resultArrayString.append(resultArrayString.first!)
        }
        let resultString = resultArrayString.joined(separator: " -> ")
        return resultString
    }

}
