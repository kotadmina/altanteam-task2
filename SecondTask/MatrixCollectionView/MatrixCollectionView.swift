//
//  MatrixCollectionView.swift
//  SecondTask
//
//  Created by Sher Locked on 08.11.17.
//  Copyright © 2017 sherlocked. All rights reserved.
//

import UIKit

class MatrixCollectionView: UIView {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var leftStackView: UIStackView!
    
    var size: Int = 5 {
        didSet {
            for i in 1...size {
                let text = String(i)
                let topLabel = UILabel()
                topLabel.textAlignment = .center
                topLabel.text = text
                topStackView.addArrangedSubview(topLabel)
                let leftLabel = UILabel()
                leftLabel.textAlignment = .center
                leftLabel.text = text
                leftStackView.addArrangedSubview(leftLabel)
            }
        }
    }
    var values: [[Int]] = []
    var result: [Int]? 
    
    class func instanceFromNib() -> MatrixCollectionView {
        let nib = UINib(nibName: "MatrixCollectionView", bundle: nil)
        let view = nib.instantiate(withOwner: nil, options: nil)[0] as! MatrixCollectionView
        let cellNib = UINib(nibName: "CollectionViewCell", bundle: nil)
        view.collectionView.register(cellNib, forCellWithReuseIdentifier: "CollectionCell")
        view.collectionView.delegate = view
        view.collectionView.dataSource = view
        view.collectionView.layer.borderWidth = 2.0
        view.collectionView.layer.borderColor = UIColor.black.cgColor
        return view
    }
    

}

extension MatrixCollectionView: UICollectionViewDelegate {
    
}

extension MatrixCollectionView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return size * size
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionViewCell
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.black.cgColor
        if values.count >= size {
            let itemValue = values[indexPath.row / size][indexPath.row % size]
            cell.configure(weight: String(itemValue))
        } else {
            cell.configure(weight: "")
        }
        
        let fromCity = indexPath.row / size + 1
        if let result = result {
            for (i, item) in result.enumerated() {
                if item == fromCity {
                    let percent = Double(i + 1) / Double(size)
                    let toCity = result[(i + 1) % size]
                    if (indexPath.row % size) + 1 == toCity {
                        cell.backgroundColor = UIColor.blue.withAlphaComponent(CGFloat(percent))
                    }
                    break
                }
            }
        }
        
        return cell
    }
    
}

extension MatrixCollectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let fullWidth = collectionView.frame.width
        let fullHeight = collectionView.frame.height
        return CGSize(width: fullWidth / CGFloat(size), height: fullHeight / CGFloat(size))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}










