//
//  MatrixRandomizer.swift
//  SecondTask
//
//  Created by Sher Locked on 09.11.17.
//  Copyright © 2017 sherlocked. All rights reserved.
//

import Foundation

class MatrixRandomizer {
    
    static func createRandomMatrix(size: Int, symmetric: Bool) -> [[Int]] {
        
        let emptyRow = [Int].init(repeating: 0, count: size)
        var matrix = [[Int]].init(repeating: emptyRow, count: size)
        
        for i in 0..<size {
            for j in 0..<size {
                if i == j {
                    continue
                }
                
                let randomValue = Int(arc4random_uniform(10)) + 1
                matrix[i][j] = randomValue
                if symmetric {
                    matrix[j][i] = randomValue
                }
            }
        }
        
        return matrix
        
    }
    
}
