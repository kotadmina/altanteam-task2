//
//  SecondViewController.swift
//  SecondTask
//
//  Created by Sher Locked on 07.11.17.
//  Copyright © 2017 sherlocked. All rights reserved.
//

import UIKit
import RealmSwift

class SecondViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let cellIdentifier = "TableCell"
    private let cellNibName = "TableViewCell"
    private let storyboardName = "Main"
    private let infoVCIdentifier = "infoVC"
    
    var results = [TSPData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let realm = try! Realm()
        let resultsRealm = realm.objects(TSPDataRealm.self)
        results = resultsRealm.map { TSPData(realmObject: $0) }
        tableView.reloadData()
    }
    
    func configureTable() {
        let nib = UINib(nibName: cellNibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
    }

}

extension SecondViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let infoVC = storyboard.instantiateViewController(withIdentifier: infoVCIdentifier) as! InfoViewController
        self.navigationController?.pushViewController(infoVC, animated: true)
        let result = results[indexPath.row]
        infoVC.result = result
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}

extension SecondViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! TableViewCell
        cell.selectionStyle = .none
        let result = results[indexPath.row]
        cell.configure(date: result.date, count: result.size)
        return cell
    }
}

