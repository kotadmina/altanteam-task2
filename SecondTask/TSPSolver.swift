//
//  TSPSolver.swift
//  SecondTask
//
//  Created by Sher Locked on 11.11.17.
//  Copyright © 2017 sherlocked. All rights reserved.
//

import Foundation

class TSPSolver {
    
    static func solve(with matrix: [[Int]]) -> TSPData {
        let date = Date()
        var permutation: [Int]? = []
        guard matrix.count > 1 else {
            let data = TSPData(size: matrix.count,
                               date: date,
                               aMatrix: matrix,
                               resultArray: [1],
                               distance: 0)
            return data
        }
        for i in 2...matrix.count {
            permutation?.append(i)
        }
        
        var minDistance: Int?
        var result: [Int]?
        
        while permutation != nil {
            ///
            var tempResult = [1]
            tempResult.append(contentsOf: permutation!)
            var fullDistance = 0
            for i in 0..<tempResult.count {
                let startCity = tempResult[i]
                let endCity = tempResult[(i + 1) % tempResult.count]
                let distance = matrix[startCity - 1][endCity - 1]
                fullDistance += distance
            }
            if let min = minDistance {
                if fullDistance < min {
                    minDistance = fullDistance
                    result = tempResult
                }
            } else {
                minDistance = fullDistance
                result = tempResult
            }
            
            permutation = nextPermutation(array: permutation!)
        }
        
        let data = TSPData(size: matrix.count,
                           date: date,
                           aMatrix: matrix,
                           resultArray: result ?? [],
                           distance: minDistance ?? 0)
        return data
    }
    
    private static func nextPermutation(array: [Int]) -> [Int]? {
        var i = array.count - 1
        
        while(i > 0 && array[i - 1] >= array[i]) {
            i -= 1
        }
        if (i <= 0) {
            return nil
        }
        var j = array.count - 1
        while (array[j] <= array[i - 1]) {
            j -= 1
        }
        var resultArray = array
        resultArray.swapAt(i - 1, j)
        
        j = array.count - 1
        while (i < j) {
            resultArray.swapAt(i, j)
            i += 1
            j -= 1
        }
        return resultArray
    }
    
}
