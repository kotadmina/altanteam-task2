//
//  TSPdata.swift
//  SecondTask
//
//  Created by Sher Locked on 07.11.17.
//  Copyright © 2017 sherlocked. All rights reserved.
//

import Foundation
import RealmSwift

class TSPData {
    
    var size: Int
    var date: Date
    var aMatrix: [[Int]]
    var resultArray: [Int]
    var distance: Int
    var id: UUID
    
    init(size: Int, date: Date, aMatrix: [[Int]], resultArray: [Int], distance: Int) {
        self.size = size
        self.date = date
        self.aMatrix = aMatrix
        self.resultArray = resultArray
        self.distance = distance
        self.id = UUID()
    }
    
    init(realmObject: TSPDataRealm) {
        self.size = realmObject.size
        self.date = realmObject.date
        self.id = UUID(uuidString: realmObject.id)!
        self.resultArray = Array(realmObject.resultArray)
        self.distance = realmObject.distance
        let rowsRealm = Array(realmObject.aMatrix)
        self.aMatrix = [[Int]]()
        for rowRealm in rowsRealm {
            let row = Array(rowRealm.row)
            self.aMatrix.append(row)
        }
    }
}

class TSPDataRealm: Object {
    
    @objc dynamic var size: Int = 0
    @objc dynamic var date: Date = Date()
    var aMatrix = List<MatrixRowRealm>()
    var resultArray = List<Int>()
    @objc dynamic var distance: Int = 0
    @objc dynamic var id: String = UUID().uuidString
    
    convenience init(data: TSPData) {
        
        self.init()
        self.size = data.size
        self.date = data.date
        var rowsRealm: [MatrixRowRealm] = []
        for row in data.aMatrix {
            let rowRealm = MatrixRowRealm()
            rowRealm.row.append(objectsIn: row)
            rowsRealm.append(rowRealm)
        }
        self.aMatrix.append(objectsIn: rowsRealm)
        self.resultArray.append(objectsIn: data.resultArray)
        self.distance = data.distance
        self.id = data.id.uuidString
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

class MatrixRowRealm: Object {
    let row = List<Int>()
}

