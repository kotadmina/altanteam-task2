//
//  TableViewCell.swift
//  SecondTask
//
//  Created by Sher Locked on 07.11.17.
//  Copyright © 2017 sherlocked. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    func configure(date: Date, count: Int) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        let dateString = formatter.string(from: date)
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.string(from: date)
        dateLabel.text = dateString
        timeLabel.text = timeString
        infoLabel.text = String(count)
    }
    
}
